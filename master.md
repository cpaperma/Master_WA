# S1

## WEB analyste 
Pool: Base de données avancées, reporting decisionnelle, droit des données, marketing quantitatifs, fondamentaux du web.

1. 
2. Base de données 
3. Programmation serveur
4. ***(truc e-marketing)


## Tronc commun
* UE1: Statistique exploratoire (Analyse de données 1)
* UE2: Programmation fonctionnelle en R
* UE3: Anglais/Gestion/Marketing
* UE4: Remise à niveau (prescrire) maths/info : bases de système d'info et bases de BD; stat élémentaires


# S2

## WEB analyste 
Pool: Web et référencement, e-marketing, programmation et base de données, entreprots de données, analyse et traitement de l'information économique.

1. Projet d'informatique de M1 (Projet d'agrégation)
2. Agrégation en python (côté serveur)
3. JavaScript (Ajax/visualisation de données)
4. e-marketing
	
## Tronc commun
* UE1    Fouille de données
* UE2    Analyse de données 2
* UE3    Anglais/Gestion/Marketing
* UE4    RO entreprise
* UE8    Logiciel stats et séminaire pro



# S3
Pool: programmation web, fouille de données avancée, web et réseau, analyse de trafic web, marketing relation client et communauté, projet e-marketing, 
économie et management stratégiques de l'information, séminaire pro.

1. Projet d'informatique de M2
2. Agrégation côté client
3. Web et référencement
4. Séminaire technologie du web: présentation et démonstration d'un outils moderne (base de données noSQL, framework de dev web, ect...) par les étudiants.
5. Outils d'analyse de trafic web (pro?)
6. *** (truc e-marketing)
7. Droits des données 
8. SEO (pro?)



