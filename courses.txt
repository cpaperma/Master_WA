Pour info et avant réunion services, voici (ci-après) une description des cours de MIASHS tronc commun et WA avec les enseignements à pourvoir. 
Pour les professionnels, 
* il y a un pro à trouver pour "Entrepôts de données - Big Data" pour remplacer B. Lauruol
* "Analyse de trafic Web" est assuré par Aurélien Bouchard (la redoute), il faudrait vérifier qu'il continue. 
* J'ai aussi l'entreprise Optimal Ways qui souhaite intervenir sur ces sujets. Cela pourrait être pour remplacer Aurélien Bouchard au cas où ou bien dans l'UE8 "séminaires professionnels" du S3. 
Slawek, on peut en discuter quand tu veux et rédiger des mails en conséquence.
Rémi

******************************************************************************** 
*** Bref descriptif des cours du S1 

* UE1 (tronc commun) : algorithmes fondamentaux de fouille de données 
Enseignant info à définir


Le clustering ou segmentation : les k-moyennes, la classification hiérarchique ; méthodes de réduction de dimension : analyse en composantes principales ; 
La classification supervisée : k-plus proches voisins, arbres de décision, classeur de Bayes, méthodes d'évaluation (train and test, cross validation, bootstrap). 
Les algorithmes sont mis en pratique sur des études de cas en R. 

* UE2 (tronc commun) A. Amiri : statistique inférentielle 

Régression linéaire simple  : estimation, intervalles de confiance et tests. 
Régression linéaire multiple  : estimation, régions de confiance, tests, sélection de modèles ; estimation par maximum de vraisemblance avec une application à l'estimation du modèle linéaire. 

* UE3 (tronc commun) S. Jougan et D. Warzecha : anglais 
en groupe de niveau 

* UE4 (tronc commun) fondamentaux du web 
Enseignant info à définir ou pro
Réseau Internet et protocoles, bases des langages html et css, introduction à php, notions de session et cookies, introduction aux algorithmes du web (pagerank) 

* UE5 (WA) bases de données avancées
Enseignant info à définir (Slawek ?)
Compléments sur bases de données relationnelles : contraintes d'intégrité, concurrence, droits et vues. 
Introduction aux bases décisionnelles et SQL Cube
Les bases NoSQL : répartition, Map Reduce, modèle clé-valeur, modèle document et modèle graphe 

* UE6 (WA) reporting décisionnel 
B. Fiers (pro)
L’objectif est de présenter un outil de reporting et d’en acquérir la maîtrise opérationnelle. Un second objectif est d’introduire les spécifités de l’interrogation de données dans un cadre décisionnel. Ceci dans l’objectif de bien comprendre les spécifités des données décisionnelles et de leur place dans le système d’information d’entreprise. En particulier, l’accent sera porté sur les modèles métiers d’analyse. 
La mise en oeuvre se fait avec Qlikview avec le développement d'une application de l'accès à la couche d'analyse. 

* UE7 (WA) C. Cren Denis : droit des données et de l'Internet 

Protection des données personnelles et Internet : cadre juridique de protection des données personnelles, protection des données en ligne. 
Internet et la sécurité des personnes : la cybersurveillance dans l'entreprise, la cybercriminalité, la responsabilité des acteurs Internet 

* UE8 (WA) E. Delattre : marketing quantitatif 

Méthodes d'analyse de données dans le cadre d'une étude marketing. 
Applications sur des cas simplifiés (enquêtes de satisfaction, études d'image, ...) avec les logiciels sphinx et statistica.

*** Bref descriptif des cours du S2

* UE1 (tronc commun)  programmation fonctionnelle et programmation R 

Enseignant info à définir

Programmation fonctionnelle R : bases d’algorithmique, définition de fonctions, programmation vectorielle, éléments spécifiques de R (structures de données : listes, hashtables, dataframes, matrices, manipulation de dates, objets S4, environnements passés par référence) 

* UE2 (tronc commun) S. Dabo : statistique asymptotique 

* UE3 (tronc commun) S. Dabo : logiciels statistiques - SAS 

Ce cours est une initiation au logiciel SAS à travers une présentation de ces diverses fonctionnalités. Il permet de comprendre la logique du langage utilisé sous SAS, de programmer et d'effectuer des analyses statistiques sous SAS.

* UE4 (tronc commun) S. jougan et D. Warzecha : anglais (en groupe de niveau) 

* UE5 (tronc commun) : TER, séminaires, ateliers et stage de 2 à 4 mois en entreprise

12h de cours (nicolas@optimals ways). En 2017, une introduction au métier et aux outils du Web analyste par Aurélien Bouchard (pro)

* UE6 (WA) J Web et référencement

Enseignant info à définir (Fabien ?)
Les graphes qui interviennent dans le web modélisent la transmission d'information. Le but premier de ce cours est d'introduire le concept de graphe, d'étudier la structure de ces graphes (en particulier celui du web) ainsi que les mécanismes de leur formation. En s'appuyant sur ces connaissances, on étudiera ensuite les notions de navigation et de contagion.

* UE7 (WA) J. Elaut : e-marketing 

J'aborde les notions de base en marketing Création de valeur perçue Le marché et sa connaissance, concurrence Les études de marché avec un point sur la partie étude qualitative (neuromarketing) Ensuite, j'aborde les 4 P et les impacts du web sur ces derniers. J'aurai aussi l'occasion d'aborder avec les étudiants l'outil de tracking web (Xiti), le webmarketing (SEM, SMO, SEO, SEA)

* UE8 (WA) programmation et bases de données 
Enseignant info à définir (Mika ?)
Un très grand nombre de pages du web sont construites à la volée suite à une requête de l'utilisateur. La nécessité de réaliser une extraction d’informations depuis des bases de données pour produire des pages internet est essentielle. Dans ce cours nous nous concentrons sur la programmation coté serveur et sur les éléments qui permettent l'interaction avec des bases de données à partir d'applications web.

* UE9 (WA) entrepôts de données 
Bertrand Lauruol (pro) arrête cette année. Donc trouver pro ou orienter sur un cours BD Big Data ?

@Sophie Dabo : Orienter le cours vers Big Data ? 

L’objectif est de présenter les approches de construction d’entrepôt de données permettant de répondre aux besoins de maîtrise de gros volumes de données et de présenter les principaux domaines d’usages au sein des organisations. Il introduit également les évolutions liées à l'apparition de la problématique "Big Data" pour les données volumineuses et en ligne.

* UE10 (WA) J.P. Boussemart : analyse et traitement de l'information économique

Ce cours est consacré à la théorie et à la pratique des nombres indices. Les indices sont des nombres analysant les évolutions chronologiques des séries statistiques de paniers regroupant différents biens ou services mesurés dans des unités différentes. Ils permettent de dissocier l'évolution globale de la valeur d'un panier de biens exprimée en valeur (euros) en un effet global prix et un effet global volume (ou quantité) à partir des observations sur les composantes individuelles de ce panier. Ces nombres indices sont ensuite mobilisés pour mesurer la performance globale d'une activité économique en la décomposant en deux effets : la productivité globale des facteurs et l'avantage prix. Le cours développe plusieurs exemples pratiques et pédagogiques. Finalement, leur mise en application dans le cadre d'une base de données regroupant 63 secteurs d'activité de l'économie américaine sur une période de plus de 25 ans permet aux étudiants de se familiariser à l'interprétation économique de ce type d'outils et d'aboutir à des recommandations opérationnelles pour la prise de décision. 

@ J.P. B: COntacter pour la BD.


*** Bref descriptif des cours du S3

* UE1 (WA) Programmation Web   
Enseignant info à définir (potentiellement à partager avec un pro)

Programmation javascript : Objets en javascript, patterns de programmation, jquery, ajax, production de documents graphiques orientée document (d3js), bases de données javascript pour les grand volumes de données (mongoDB), serveur javascript et MVC (node.js).

* UE2 (WA) Fouille de données avancées

Enseignant info à définir

Le but général est d'affronter des jeux de données non déjà formatés, volumineux, mélangeant des attributs de différentes natures, et réaliser des tâches de fouille dans ce contexte. Les méthodes de de préparation de données et de visualisation d'information sont présentées et traitées sur des cas réels. Enfin, les différents types de systèmes de recommandation (filtrage par contenu, filtrage collaboratif, filtrage hybride) sont étudiés.

*UE3 (WA) Web et réseaux (ouvert à un externe)

$$$ Extraction de données à partir du Web 

Enseignant info à définir

Contenu __adaptable__ qui peut être orienté technos : html5 et réseaux sociaux, html5 et Web des données, Web sémantique ou orienté vers les réseaux sociaux et des algos comme clustering et recommandation




* UE4 (WA) Analyse de trafic Web    

Aurélien Bouchard (pro)

Les cours de Digital Analytics dispensent les bases de l’analyse des performances web, notamment pour des sites e-commerce. Les thématiques abordées dans ce cour permettent d’appréhender, à travers une approche théorique et pratique, les principes et les fondements de la digital analyse : c’est-à-dire en partant de l’analyse des sources de trafic en passant par les modèles d’attribution, l’A/B testing et la navigation jusqu’à l’analyse de l’entonnoir de conversion

* UE5 (WA) marketing ? Gestion relation client et communautés

* UE6 (WA) E. Delattre : Projet e-marketing

Le cours de projet e-marketing vise à appréhender l’importance de la planification de tout projet. L’objectif est de maîtriser les méthodes et outils utilisés dans le cadre d’un projet d’étude e-marketing. Le cours s’articule autour d’études de cas : efficacité d’une campagne d’e-mailing, indicateurs du media planning, mesure des performances des réseaux sociaux...

* UE7 (WA) P. Vanden Eeckaut : Economie et management stratégique de l'information

L’économie est de plus en plus orientée vers des biens à contenu numérique. Ce cours identifie la structure de marché la plus probable avec ce contenu. Les questions relatives aux points suivants seront également abordées: La différenciation des produits ; les réseaux La standardisation et les coûts de changement ; le droit de propriété et piratage ; les marchés bifaces et l’économie collaborative. 


* UE8 (WA) séminaires professionnels

$$$ 24h 

Comun en 2017 avec STAT mais peut être spécifique au WA.
